# qsim

`qsim` is package for simulating quantum computations. The package implements the basic gates for the Pauli matrices (`X`, `Y`, `Z`), the Hadamard matrix (`H`), the controlled NOT operator (`CNOT`), and the Toffoli operator (`Toffoli`).

## TODO

- [x] implement measurement simulator for non-deterministic states
- [ ] implement a quantum circuit interpreter

## Design

### `StateSpace`

The `StateSpace` interface provides a broad abstraction for manipulating the underlying vectors and matrices involved in running simulated QC calculations. Three important objects implement `StateSpace`: the `QBit`, the `Register`, and the `Operator`.

The `QBit` provides a struct layout suitable for describing the initial preparation of qbits on a simulated quantum machine. Each of `|0>`, `|1>`, `|+>`, and `|->` are supplied as sentinel values. The underlying datastructure is a second-degree `AmplitudeVector`.

The `Register` provides a struct layout which represents an array of `QBit`s; the underlying data, however, are not `QBit` instances but an n-degree `AmplitudeVector`. This data may model a superposition of qbits in the abstract `StateSpace`.

The `Operator` provides a struct layout to house the unitary matrix applied in modifications of the abstract `StateSpace`. Each QC gate in `qsim` is implemented by applying the appropriate `Operator`.

All `StateSpace` implementations supply the methods:

```go
type StateSpace interface {
  // Amplitudes returns the underlying data for the state space
  Amplitudes() AmplitudeVector
  // Equal compares the underlying data between state spaces, checking for logical equivalence up to an error of 1e-15
  Equal(StateSpace) bool
  // RawAmplitudes returns the underlying data for the state space as complex numbers
  RawAmplitudes() []complex128
  // Size gives the row count of the underlying data. For QBit and Register implementations the returned value implies an `n`x`1` matrix. Note that, for the Operator implementation, the returned value does not give the number of columns. As an Operator has an underlying UnitaryMatrix, the returned value `n` implies an `n`x`n` matrix.
	Size() int
}
```

### Growing the `StateSpace`

One challenge in managing the data manipulated by QC -- maybe the key challenge -- is how to model the catenation of additional qbits. `qsim` handles this challenge by defining a tensor product for the `StateSpace`.

Mathematically, given a pair of 2x1 matrices representing single qbits, the tensor product defined here produces a 4x1 matrix representing the pair. In general, N `QBit`s tensored in this manner produce a 2Nx1 `Register`.

Since both `QBit` and `Register` implement `StateSpace`, new qbits can be added to the register when necessary to continue applying larger transformations, such as `CNOT` or `Toffoli` gates.

Note that the tensor product described here is associative, i.e., `Tensor(Tensor(OneQ, NulQ), OneQ)` equals `Tensor(OneQ,Tensor(NulQ,OneQ))`.

### Handling complex data

In quantum theory, both the amplitude vector values and operator matrix values are complex numbers. `gonum` does not define multiplication on complex matrices out-of-the-box; `qsim` therefore stores the underlying values in two separate matrices, representing the real and imaginary components, respectively.

The following relation allows for computing complex matrices using separate component matrices:

```
(A + Bi) * (C + Di) = (A*C - B*D) + (A*D + B*C)i
```

Matrix multiplication occurs on real-valued matrices, and results are returned to the appropriate component matrix via the bookkeeping above.