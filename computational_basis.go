package qsim

import (
	"bytes"
	"fmt"
	"math"
)

// ClassicalBit represents binary bits in classical computing
type ClassicalBit int

const (
	// ClassicalNul is the binary bit 0 from classical computing
	ClassicalNul ClassicalBit = 0
	// ClassicalOne is the binary bit 1 from classical computing
	ClassicalOne ClassicalBit = 1
)

// ComputationalBasis represents the classical bits measured out of a quantum register
type ComputationalBasis []ClassicalBit

// NewComputationalBasis creates a bit string for numeral n zero padded on the left
func NewComputationalBasis(n int, size int) ComputationalBasis {
	var basis ComputationalBasis
	for _, bit := range fmt.Sprintf("%b", n) {
		switch bit {
		case '1':
			basis = append(basis, ClassicalOne)
		case '0':
			basis = append(basis, ClassicalNul)
		}
	}

	return leftPad(basis, size)
}

func leftPad(basis ComputationalBasis, size int) ComputationalBasis {
	padLength := int(math.Sqrt(float64(size))) - len(basis)
	for i := 0; i < padLength; i++ {
		basis = append(ComputationalBasis{0}, basis...)
	}
	return basis
}

func (bit ClassicalBit) String() string {
	return fmt.Sprint(int(bit))
}

// String provides the string representation of a ComputationalBasis
func (basis ComputationalBasis) String() string {
	var b bytes.Buffer

	for _, bit := range basis {
		b.WriteString(bit.String())
	}

	return b.String()
}

// Equal determines if the computational basis has the same bit values as its comparator
func (basis ComputationalBasis) Equal(other ComputationalBasis) bool {
	return basis.String() == other.String()
}
