package qsim

import "testing"

func TestComputationalBasisString(t *testing.T) {
	basis := ComputationalBasis{1, 1, 0}
	if basis.String() != "110" {
		t.Fail()
	}
}

func TestComputationalBasisEqual(t *testing.T) {
	basis1 := ComputationalBasis{1, 1, 0}
	basis2 := ComputationalBasis{1, 1, 0}
	basis3 := ComputationalBasis{1, 0, 0}

	if !basis1.Equal(basis2) {
		t.Errorf("expected %s to equal %s", basis1, basis2)
	}

	if basis2.Equal(basis3) {
		t.Errorf("expected %s to equal %s", basis2, basis3)
	}
}
