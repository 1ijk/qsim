package qsim

import (
	"gitlab.com/1ijk/qsim/linalg"
)

var (
	// OperatorI is the identity matrix.
	//
	// |1 0|
	// |0 1|
	//
	OperatorI = Operator{linalg.BuildMatrix(2, 2, []complex128{
		1, 0,
		0, 1,
	})}

	// OperatorX is the permutation matrix for the 1 qbit X gate
	//
	// |0 1|
	// |1 0|
	OperatorX = Operator{linalg.BuildMatrix(2, 2, []complex128{
		0, 1,
		1, 0,
	})}
	// OperatorY is the permutation matrix for the 1 qbit Y gate
	//
	// |0 -i|
	// |i  0|
	OperatorY = Operator{linalg.BuildMatrix(2, 2, []complex128{
		0, complex(0, -1),
		complex(0, 1), 0,
	})}
	// OperatorZ is the permutation matrix for the 1 qbit Z gate
	//
	// |1  0|
	// |0 -1|
	OperatorZ = Operator{linalg.BuildMatrix(2, 2, []complex128{
		1, 0,
		0, -1,
	})}
	// OperatorH is the permutation matrix for the 1 qbit H gate
	//
	// |1/√2  1/√2|
	// |1/√2 -1/√2|
	OperatorH = Operator{linalg.BuildMatrix(2, 2, []complex128{
		HScalar, HScalar,
		HScalar, -HScalar,
	})}
	// OperatorCNOT is the permutation matrix for the 2 qbit CNOT gate
	//
	// |1 0 0 0|
	// |0 1 0 0|
	// |0 0 0 1|
	// |0 0 1 0|
	OperatorCNOT = Operator{linalg.BuildMatrix(4, 4, []complex128{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 0, 1,
		0, 0, 1, 0,
	})}
	// OperatorTransposedCNOT is the permutation matrix for the 2 qbit CNOT gate,
	// with the first qbit set as the target and the second as the control.
	//
	// |0 1 0 0|
	// |1 0 0 0|
	// |0 0 1 0|
	// |0 0 0 1|
	OperatorTransposedCNOT = Operator{linalg.BuildMatrix(4, 4, []complex128{
		0, 1, 0, 0,
		1, 0, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	})}
	// OperatorToffoli is the permutation matrix for the 3 qbit Toffoli gate
	//
	// |1 0 0 0 0 0 0 0|
	// |0 1 0 0 0 0 0 0|
	// |0 0 1 0 0 0 0 0|
	// |0 0 0 1 0 0 0 0|
	// |0 0 0 0 1 0 0 0|
	// |0 0 0 0 0 1 0 0|
	// |0 0 0 0 0 0 0 1|
	// |0 0 0 0 0 0 1 0|
	OperatorToffoli = Operator{linalg.BuildMatrix(8, 8, []complex128{
		1, 0, 0, 0, 0, 0, 0, 0,
		0, 1, 0, 0, 0, 0, 0, 0,
		0, 0, 1, 0, 0, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0, 0,
		0, 0, 0, 0, 1, 0, 0, 0,
		0, 0, 0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 1,
		0, 0, 0, 0, 0, 0, 1, 0,
	})}
)

// X quantum gate applies the Pauli X matrix. It can be used as an equivalent to the classical NOT gate
func X(phi StateSpace) StateSpace {
	return gate(OperatorX, phi)
}

// Y quantum gate applies the Pauli Y matrix.
func Y(phi StateSpace) StateSpace {
	return gate(OperatorY, phi)
}

// Z quantum gate applies the Pauli Z matrix.
func Z(phi StateSpace) StateSpace {
	return gate(OperatorZ, phi)
}

// H quantum gate applies the Hadamard transform to the state space. This allows for a superposition of the classical 0 and 1 values.
func H(phi StateSpace) StateSpace {
	return gate(OperatorH, phi)
}

// CNOT quantum gate performs a "controlled NOT" to the state space.
// On a 2-qbit system, this applies a NOT to the second qbit if and only if the first qbit has classical 1 value.
// Note that the CNOT allows for the superposition of states, leading to an indeterminate action on the target qbit.
func CNOT(phi StateSpace) StateSpace {
	return gate(OperatorCNOT, phi)
}

// TransposeCNOT quantum gate performs a "controlled NOT" to the state space.
// The transposed version places the control on the second qubit and the target on the first.
func TransposeCNOT(phi StateSpace) StateSpace {
	return gate(OperatorTransposedCNOT, phi)
}

// Toffoli quantum gate performs a reversible, multi-qbit controlled NOT to the state space.
// On a 3-qbit system, this applies a NOT to the last qbit if and only if the first two qbits both have classical 1 value.
// Note that the Toffoli allows for the superposition of states, leading to an indeterminate action on the target qbit.
func Toffoli(phi StateSpace) StateSpace {
	return gate(OperatorToffoli, phi)
}

func gate(op Operator, phi StateSpace) StateSpace {
	result, err := op.Apply(phi)
	if err != nil {
		panic(err)
	}
	return result
}
