package qsim

import (
	"testing"
)

func TestX(t *testing.T) {
	if res := X(NulQ); !res.Equal(OneQ) {
		t.Fail()
	}
	if res := X(OneQ); !res.Equal(NulQ) {
		t.Fail()
	}
	if res := X(X(NulQ)); !res.Equal(NulQ) {
		t.Fail()
	}
}

func TestH(t *testing.T) {
	if res := H(NulQ); !res.Equal(PosQ) {
		t.Fail()
	}
	if res := H(OneQ); !res.Equal(NegQ) {
		t.Fail()
	}
	if res := H(H(NulQ)); !res.Equal(NulQ) {
		t.Errorf("expected Hadamard to be involutary, got %s following second application", res)
	}
}

func TestCNOT(t *testing.T) {
	if res := CNOT(NewRegister(NulQ, NulQ)); !res.Equal(NewRegister(NulQ, NulQ)) {
		t.Fail()
	}
	if res := CNOT(NewRegister(NulQ, OneQ)); !res.Equal(NewRegister(NulQ, OneQ)) {
		t.Fail()
	}
	if res := CNOT(NewRegister(OneQ, NulQ)); !res.Equal(NewRegister(OneQ, OneQ)) {
		t.Fail()
	}
	if res := CNOT(NewRegister(OneQ, OneQ)); !res.Equal(NewRegister(OneQ, NulQ)) {
		t.Fail()
	}
	if res := CNOT(NewRegister(PosQ, PosQ)); !res.Equal(NewRegister(PosQ, PosQ)) {
		t.Errorf("expected |++> to become |++>, got %s", res)
	}
	if res := CNOT(NewRegister(PosQ, NegQ)); !res.Equal(NewRegister(NegQ, NegQ)) {
		t.Errorf("expected |+-> to become |-->, got %s", res)
	}
	if res := CNOT(NewRegister(NegQ, PosQ)); !res.Equal(NewRegister(NegQ, PosQ)) {
		t.Errorf("expected |-+> to become |-+>, got %s", res)
	}
	if res := CNOT(NewRegister(NegQ, NegQ)); !res.Equal(NewRegister(PosQ, NegQ)) {
		t.Errorf("expected |--> to become |+->, got %s", res)
	}
}

var (
	toffoliExamples = []struct {
		result StateSpace
		expect Register
	}{
		{
			result: Toffoli(NewRegister(NulQ, NulQ, NulQ)),
			expect: NewRegister(NulQ, NulQ, NulQ),
		},
		{
			result: Toffoli(NewRegister(OneQ, NulQ, NulQ)),
			expect: NewRegister(OneQ, NulQ, NulQ),
		},
		{
			result: Toffoli(NewRegister(NulQ, OneQ, NulQ)),
			expect: NewRegister(NulQ, OneQ, NulQ),
		},
		{
			result: Toffoli(NewRegister(OneQ, OneQ, NulQ)),
			expect: NewRegister(OneQ, OneQ, OneQ),
		},
	}
)

func TestToffoli(t *testing.T) {
	for _, example := range toffoliExamples {
		if !example.result.Equal(example.expect) {
			t.Errorf("expected %s, got %s", example.expect, example.result)
		}
	}
}
