package linalg

import "gonum.org/v1/gonum/mat"

func Equal(a, b Matrix) bool {
	return mat.CEqualApprox(a, b, 1e-15)
}
