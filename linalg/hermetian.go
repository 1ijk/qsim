package linalg

import (
	"math/cmplx"

	"gonum.org/v1/gonum/mat"
)

func hermetian(m Mutable) mat.CMatrix {
	cols, rows := m.Dims()

	result := mat.NewCDense(rows, cols, nil)

	for row := 0; row < cols; row++ {
		for col := 0; col < rows; col++ {
			result.Set(row, col, cmplx.Conj(m.At(col, row)))
		}
	}
	return result
}
