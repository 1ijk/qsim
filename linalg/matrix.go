package linalg

import (
	"bytes"
	"fmt"

	"gonum.org/v1/gonum/mat"
)

type Matrix struct {
	data *mat.CDense
}

func BuildMatrix(rows, columns int, data []complex128) Matrix {
	return Matrix{mat.NewCDense(rows, columns, data)}
}

func NewMatrix(rows, columns int) Matrix {
	return Matrix{mat.NewCDense(rows, columns, nil)}
}

func (v Matrix) Dims() (int, int) {
	return v.data.Dims()
}

func (v Matrix) At(i, j int) complex128 {
	return v.data.At(i, j)
}

func (v Matrix) Set(i, j int, data complex128) {
	v.data.Set(i, j, data)
}

func (v Matrix) H() mat.CMatrix {
	return hermetian(v.data)
}

func (v Matrix) String() string {
	var b bytes.Buffer

	rows, cols := v.Dims()

	b.WriteString("[")
	for row := 0; row < rows; row++ {
		b.WriteString("\n")
		for col := 0; col < cols; col++ {
			b.WriteString(fmt.Sprintf(" %v ", v.At(row, col)))
		}

		if row == rows {
			b.WriteString("\n")
		}
	}
	b.WriteString("]")

	return b.String()
}
