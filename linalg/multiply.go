package linalg

import (
	"fmt"

	"gonum.org/v1/gonum/mat"
)

var ErrDimensionMismatch = fmt.Errorf("matrix dimensions invalid for operation")

type Mutable interface {
	mat.CMatrix
	Set(int, int, complex128)
}

func Multiply(left, right Mutable) (Mutable, error) {
	var result Mutable

	m, p := left.Dims()
	p, n := right.Dims()

	result = NewMatrix(m, n)

	for row := 0; row < m; row++ {
		for col := 0; col < n; col++ {
			for elem := 0; elem < p; elem++ {
				prod := (left.At(row, elem) * right.At(elem, col))
				value := result.At(row, col) + prod
				result.Set(row, col, value)
			}
		}
	}

	return result, nil
}
