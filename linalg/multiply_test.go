package linalg

import (
	"testing"

	"gonum.org/v1/gonum/mat"
)

var (
	multiplyExamples = []struct {
		left   Matrix
		right  Matrix
		expect Matrix
	}{
		//{
		//	left: Matrix{mat.NewCDense(2, 2, []complex128{
		//		complex(1, 0), complex(0, 1),
		//		complex(0, 1), complex(1, 0),
		//	})},
		//	right: Matrix{mat.NewCDense(2, 2, []complex128{
		//		complex(0, 1), complex(1, 0),
		//		complex(1, 0), complex(0, 1),
		//	})},
		//	expect: Matrix{mat.NewCDense(2, 2, []complex128{
		//		complex(0, 2), complex(0, 0),
		//		complex(0, 0), complex(0, 2),
		//	})},
		//},
		{
			left: Matrix{mat.NewCDense(2, 2, []complex128{
				complex(1, 0), complex(0, 1),
				complex(0, 1), complex(1, 0),
			})},
			right: Matrix{mat.NewCDense(2, 1, []complex128{
				complex(0, 1), complex(1, 0),
			})},
			expect: Matrix{mat.NewCDense(2, 1, []complex128{
				complex(0, 2), complex(0, 0),
			})},
		},
		{
			left: Matrix{mat.NewCDense(2, 2, []complex128{
				0, 1,
				0, 0,
			})},
			right: Matrix{mat.NewCDense(2, 2, []complex128{
				0, 0,
				1, 0,
			})},
			expect: Matrix{mat.NewCDense(2, 2, []complex128{
				1, 0,
				0, 0,
			})},
		},
		{
			left: Matrix{mat.NewCDense(2, 2, []complex128{
				0, 0,
				1, 0,
			})},
			right: Matrix{mat.NewCDense(2, 2, []complex128{
				0, 1,
				0, 0,
			})},
			expect: Matrix{mat.NewCDense(2, 2, []complex128{
				0, 0,
				0, 1,
			})},
		},
	}
)

func TestUnitaryMultiply(t *testing.T) {
	for i, example := range multiplyExamples {
		result, err := Multiply(example.left, example.right)
		if err != nil {
			t.Fatal(err)
		}
		if !mat.CEqual(result, example.expect) {
			t.Errorf("example %d failed. expected %s got %s", i, example.expect, result)
		}
	}
}
