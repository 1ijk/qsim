package linalg

func Tensor(a Mutable, b Mutable) Mutable {
	var result Mutable
	arows, acols := a.Dims()
	brows, bcols := b.Dims()

	if acols*bcols == 1 {
		result = NewVector(arows * brows)
	} else {
		result = NewMatrix(arows*brows, acols*bcols)
	}

	for arow := 0; arow < arows; arow++ {
		for acol := 0; acol < acols; acol++ {
			for brow := 0; brow < brows; brow++ {
				for bcol := 0; bcol < bcols; bcol++ {
					result.Set((arow*2)+brow, (acol*2)+bcol, a.At(arow, acol)*b.At(brow, bcol))
				}
			}
		}
	}

	return result
}
