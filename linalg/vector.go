package linalg

import "gonum.org/v1/gonum/mat"

type Vector struct {
	Matrix
}

func NewVector(length int) Vector {
	return Vector{Matrix{mat.NewCDense(length, 1, nil)}}
}

func (v Vector) Len() int {
	rows, _ := v.Matrix.Dims()
	return rows
}

func (v Vector) SetVec(i int, data complex128) {
	v.Matrix.Set(i, 0, data)
}

func (v Vector) AtVec(i int) complex128 {
	return v.Matrix.At(i, 0)
}
