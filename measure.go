package qsim

import (
	"math"
	"math/cmplx"
	"math/rand"
	"time"
)

// Measure translates a quantum StateSpace into a classical ComputationalBasis
func Measure(qbits StateSpace) ComputationalBasis {
	var max int
	var sum float64

	weights := make([]float64, qbits.Len())

	for i := range weights {
		weights[i] = math.Sqrt(cmplx.Abs(qbits.AtVec(i)))
		sum += weights[i]
	}

	distance := shot() * sum

	for i, weight := range weights {
		distance -= weight
		if distance < 0 {
			max = i
			break
		}
	}

	return NewComputationalBasis(max, qbits.Len())
}

func shot() float64 {
	return rand.New(rand.NewSource(time.Now().UnixNano())).Float64()
}
