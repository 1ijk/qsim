package qsim

import "testing"

var measureExamples = []struct {
	result ComputationalBasis
	expect ComputationalBasis
}{
	{
		result: Measure(NewRegister(NulQ, NulQ)),
		expect: ComputationalBasis{0, 0},
	},
	{
		result: Measure(NewRegister(NulQ, OneQ)),
		expect: ComputationalBasis{0, 1},
	},
	{
		result: Measure(NewRegister(OneQ, NulQ)),
		expect: ComputationalBasis{1, 0},
	},
	{
		result: Measure(NewRegister(OneQ, OneQ)),
		expect: ComputationalBasis{1, 1},
	},
}

var measureNonDeterministicExamples = []struct {
	result func() ComputationalBasis
	expect ComputationalBasis
	ratio  int
}{
	{
		result: func() ComputationalBasis { return Measure(NewRegister(PosQ, NulQ)) },
		expect: ComputationalBasis{1, 0},
		ratio:  2,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(NulQ, PosQ)) },
		expect: ComputationalBasis{0, 1},
		ratio:  2,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(PosQ, PosQ)) },
		expect: ComputationalBasis{1, 1},
		ratio:  4,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(NegQ, NulQ)) },
		expect: ComputationalBasis{1, 0},
		ratio:  2,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(NulQ, NegQ)) },
		expect: ComputationalBasis{0, 1},
		ratio:  2,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(NegQ, NegQ)) },
		expect: ComputationalBasis{1, 1},
		ratio:  4,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(NegQ, PosQ)) },
		expect: ComputationalBasis{1, 1},
		ratio:  4,
	},
	{
		result: func() ComputationalBasis { return Measure(NewRegister(PosQ, NegQ)) },
		expect: ComputationalBasis{1, 1},
		ratio:  4,
	},
}

func TestMeasureDeterministic(t *testing.T) {
	for _, example := range measureExamples {
		if !example.result.Equal(example.expect) {
			t.Errorf("expected %s got %s", example.expect, example.result)
		}
	}
}

func TestMeasureNonDeterministic(t *testing.T) {
	shots := 10000

	for i, example := range measureNonDeterministicExamples {
		count := 0
		ratio := shots / example.ratio
		for i := 0; i < shots; i++ {
			if example.result().Equal(example.expect) {
				count++
			}
		}
		if (ratio - count) > (ratio / 20) {
			t.Logf("example %d", i)
			t.Error(shots, ratio, count)
		}
	}
}
