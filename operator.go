package qsim

import (
	"bytes"
	"fmt"

	"gitlab.com/1ijk/qsim/linalg"
)

// Operator is used to Apply transformations to a StateSpace.
// Operator itself implements StateSpace for convenience in running computations.
type Operator struct {
	linalg.Matrix
}

// Apply performs a transformation on the given StateSpace, returning the resulting StateSpace
func (op Operator) Apply(phi StateSpace) (StateSpace, error) {
	if r, _ := op.Dims(); r < phi.Len() {
		return op.Grow().Apply(phi)
	}

	result, err := linalg.Multiply(op, phi)
	if err != nil {
		return nil, err
	}

	return Register{linalg.Vector{(result).(linalg.Matrix)}}, nil
}

// Then allows chaining of Operators.
func (op Operator) Then(b Operator) Operator {
	return Operator{(linalg.Tensor(op, b)).(linalg.Matrix)}
}

// Grow increase the dimensionality of an operator.
func (op Operator) Grow() Operator {
	return Operator{(linalg.Tensor(op, op)).(linalg.Matrix)}
}

func (op Operator) String() string {
	var b bytes.Buffer

	rows, cols := op.Dims()

	b.WriteString("\n")

	for row := 0; row < rows; row++ {
		for col := 0; col < cols; col++ {
			b.WriteString(fmt.Sprintf(" %v ", op.At(row, col)))
		}
		b.WriteString("\n")
	}
	return b.String()
}

// Equal performs an equality check between operators
func (op Operator) Equal(op2 Operator) bool {
	return linalg.Equal(op.Matrix, op2.Matrix)
}
