package qsim

import (
	"testing"

	"gitlab.com/1ijk/qsim/linalg"
)

var composeExample = []struct {
	expect Operator
	result Operator
}{
	{
		result: OperatorH.Then(OperatorH),
		expect: Operator{linalg.BuildMatrix(4, 4, []complex128{
			0.5, 0.5, 0.5, 0.5,
			0.5, -0.5, 0.5, -0.5,
			0.5, 0.5, -0.5, -0.5,
			0.5, -0.5, -0.5, 0.5,
		})},
	},
}

func TestCompose(t *testing.T) {
	for i, example := range composeExample {
		if !example.expect.Equal(example.result) {
			t.Errorf("example %d failed: expected %s got %s", i, example.expect, example.result)
		}
	}
}
