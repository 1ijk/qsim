package qsim

import (
	"fmt"
	"math"

	"gitlab.com/1ijk/qsim/linalg"
)

// HScalar equals 1/√2, the scalar value used in Hadamard transform to provide a superposition state.
const HScalar = 1 / math.Sqrt2

var (
	// NulQ is a |0> prepared qbit
	NulQ = PrepareQuantumBit(1, 0)
	// OneQ is a |1> prepared qbit
	OneQ = PrepareQuantumBit(0, 1)
	// PosQ is a |+> prepared qbit, the result of H|0> superposition.
	PosQ = PrepareQuantumBit(HScalar, HScalar)
	// NegQ is a |-> prepared qbit, the result of H|1> superposition.
	NegQ = PrepareQuantumBit(HScalar, -HScalar)
)

// QuantumBit represents a single quantum bit
type QuantumBit struct {
	linalg.Vector
}

// PrepareQuantumBit initializes a qbit with the given amplitudes.
func PrepareQuantumBit(alpha, beta complex128) QuantumBit {
	qbit := QuantumBit{linalg.NewVector(2)}
	qbit.SetVec(0, alpha)
	qbit.SetVec(1, beta)
	return qbit
}

// Data returns a read-only copy of the underlying matrix
func (qbit QuantumBit) Data() linalg.Matrix {
	return qbit.Vector.Matrix
}

// Equal performs an equality check between StateSpaces
func (qbit QuantumBit) Equal(phi StateSpace) bool {
	return linalg.Equal(qbit.Data(), phi.Data())
}

func (qbit QuantumBit) String() string {
	if qbit.Equal(NulQ) {
		return "|0>"
	}
	if qbit.Equal(OneQ) {
		return "|1>"
	}
	if qbit.Equal(PosQ) {
		return "|+>"
	}
	if qbit.Equal(NegQ) {
		return "|->"
	}

	return fmt.Sprintf("[ %v %v ]", qbit.AtVec(0), qbit.AtVec(1))
}
