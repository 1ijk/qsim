package qsim

import "testing"

func TestString(t *testing.T) {
	if res := NulQ.String(); res != "|0>" {
		t.Errorf("incorrect result for NulQ, %s", res)
	}
	if res := OneQ.String(); res != "|1>" {
		t.Errorf("incorrect result for OneQ, %s", res)
	}
	if res := PosQ.String(); res != "|+>" {
		t.Errorf("incorrect result for PosQ, %s", res)
	}
	if res := NegQ.String(); res != "|->" {
		t.Errorf("incorrect result for NegQ, %s", res)
	}
}
