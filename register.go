package qsim

import (
	"gitlab.com/1ijk/qsim/linalg"
)

// Register represents a collection of quantum bits. Rather than represent the qbits directly,
// the Register stores the accumulated data into an expanded 2*N amplitude vector. Register adds
// qbits by applying the Tensor product to the underlying vectors.
type Register struct {
	linalg.Vector
}

// NewRegister combines QuantumBit data into a tensored vector space.
func NewRegister(qbits ...QuantumBit) Register {
	switch len(qbits) {
	case 0:
		return Register{}
	case 1:
		return Register{qbits[0].Vector}
	default:
	}

	vector := qbits[0].Vector
	for _, qbit := range qbits[1:] {
		vector = (linalg.Tensor(vector, qbit.Vector)).(linalg.Vector)
	}

	return Register{vector}
}

// Data returns the underlying matrix.
func (r Register) Data() linalg.Matrix {
	return r.Vector.Matrix
}

// Equal performs an equality check against another StateSpace
func (r Register) Equal(phi StateSpace) bool {
	return linalg.Equal(r.Data(), phi.Data())
}
