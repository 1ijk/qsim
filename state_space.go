package qsim

import (
	"gitlab.com/1ijk/qsim/linalg"
)

// StateSpace interface provides a broad abstraction for manipulating the underlying vectors and matrices involved in running simulated QC calculations.
type StateSpace interface {
	linalg.Mutable
	Equal(StateSpace) bool
	Len() int
	AtVec(int) complex128
	Data() linalg.Matrix
}
