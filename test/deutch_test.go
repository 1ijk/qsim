package qsim_test

import (
	"testing"

	"gitlab.com/1ijk/qsim"
)

func TestDeutch(t *testing.T) {
	var register qsim.StateSpace
	var measurement qsim.ComputationalBasis

	// prepare qbits
	register = qsim.NewRegister(qsim.NulQ, qsim.OneQ)

	// first step
	register = qsim.H(register)

	// second step
	register = qsim.TransposeCNOT(register)

	// third step
	register = qsim.H(register)

	// measurement
	measurement = qsim.Measure(register)

	if !measurement.Equal(qsim.ComputationalBasis{1, 1}) {
		t.Error(measurement, "should be", "11")
	}
}
